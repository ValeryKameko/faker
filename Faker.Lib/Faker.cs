﻿using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Faker.Lib
{
    public abstract class Faker : BaseFaker
    {
        public TGenerator RegisterGenerator<TGenerator>()
            where TGenerator : FakeGenerator, new()
        {
            var generator = CreateGeneratorInstance<TGenerator>();
            RegisterGenerator(generator);
            return generator;
        }

        public TGenerator CreateGeneratorInstance<TGenerator>()
            where TGenerator : FakeGenerator, new()
        {
            return new TGenerator { InnerTypeFaker = this };
        }

        protected abstract void RegisterGenerator(FakeGenerator generator);

        public FakeGenerator RegisterGenerator(Type type)
        {
            if (!typeof(FakeGenerator).IsAssignableFrom(type))
                throw new ArgumentException("type must be inherited from FakeGenerator");
            if (type.GetConstructor(Type.EmptyTypes) == null)
                throw new ArgumentException("type must have default constructor");
            var generator = CreateGeneratorInstance(type);
            RegisterGenerator(generator);
            return generator;
        }

        protected FakeGenerator CreateGeneratorInstance(Type type)
        {
            FakeGenerator generator = (FakeGenerator)Activator.CreateInstance(type);
            generator.InnerTypeFaker = this;
            return generator;
        }
    }
}

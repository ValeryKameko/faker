﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Faker.Lib
{
    public abstract class BaseFaker : IFaker
    {
        private readonly MethodInfo _genericGenerateMethod;

        public BaseFaker()
        {
            _genericGenerateMethod = GetType().GetMethods().Single(method => method.Name == nameof(Generate) && method.IsGenericMethod);
        }

        public T Generate<T>() => (T)GenerateObject(typeof(T));

        public object Generate(Type type) => _genericGenerateMethod.MakeGenericMethod(type).Invoke(this, null);

        protected abstract object GenerateObject(Type type);
    }
}

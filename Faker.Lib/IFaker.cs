﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Lib
{
    public interface IFaker
    {
        T Generate<T>();
        object Generate(Type type);
    }
}

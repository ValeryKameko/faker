﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Lib.Generators
{
    public abstract class ConcreteTypeFakeGenerator<TSupportedType> : FakeGenerator
    {
        public sealed override Type SupportedType => typeof(TSupportedType);
    }
}

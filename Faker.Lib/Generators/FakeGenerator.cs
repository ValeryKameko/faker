﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Lib.Generators
{
    public abstract class FakeGenerator : BaseFaker
    {
        protected internal IFaker InnerTypeFaker { get; internal set; }
        public abstract Type SupportedType { get; }
    }
}

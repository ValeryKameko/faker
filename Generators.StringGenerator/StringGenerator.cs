﻿using Faker.Lib.Generators;
using System;

namespace Generators.StringGenerator
{
    public sealed class StringGenerator : ConcreteTypeFakeGenerator<string>
    {
        protected override object GenerateObject(Type type) => Guid.NewGuid().ToString();
    }
}

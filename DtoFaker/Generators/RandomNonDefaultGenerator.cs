﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;

namespace DtoFaker.Generators
{
    public abstract class RandomNonDefaultGenerator<TGeneratingType> : ConcreteTypeFakeGenerator<TGeneratingType>
    {
        private readonly Random _random = new Random();

        protected override object GenerateObject(Type type)
        {
            TGeneratingType generatedValue = default;
            var equalityComparer = EqualityComparer<TGeneratingType>.Default;
            while (equalityComparer.Equals(default, generatedValue))
                generatedValue = GenerateRandomValue(_random);
            return generatedValue;
        }

        protected abstract TGeneratingType GenerateRandomValue(Random random);
    }
}

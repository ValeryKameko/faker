﻿using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace DtoFaker.Generators
{
    internal abstract class SpecialGenerator : FakeGenerator
    {
        public sealed override Type SupportedType => null;
    }
}

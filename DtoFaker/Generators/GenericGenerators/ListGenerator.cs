﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace DtoFaker.Generators.GenericGenerators
{
    public sealed class ListGenerator : FakeGenerator
    {
        private const int DefaultLength = 1;
        public int Length { get; set; } = DefaultLength;

        public override Type SupportedType => typeof(List<>);

        protected override object GenerateObject(Type type)
        {
            Type itemType = type.GenericTypeArguments.First();
            var list = Activator.CreateInstance(SupportedType.MakeGenericType(itemType)) as IList;
            IEnumerable<object> sequence = Enumerable.Range(0, Length).Select(_ => InnerTypeFaker.Generate(itemType));
            foreach (object item in sequence)
            {
                list.Add(item);
            }
            return list;
        }
    }
}

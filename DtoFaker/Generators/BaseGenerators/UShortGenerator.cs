﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class UShortGenerator : RandomNonDefaultGenerator<ushort>
    {
        protected override ushort GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[2];
            random.NextBytes(bytes);
            return BitConverter.ToUInt16(bytes);
        }
    }
}

﻿using System;
using Faker.Lib;
using Faker.Lib.Generators;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class BoolGenerator : ConcreteTypeFakeGenerator<bool>
    {
        protected override object GenerateObject(Type type) => true;
    }
}

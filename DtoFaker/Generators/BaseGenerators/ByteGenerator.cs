﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class ByteGenerator : RandomNonDefaultGenerator<byte>
    {
        protected override byte GenerateRandomValue(Random random)
        {
            byte[] generatedValue = new byte[1];
            random.NextBytes(generatedValue);
            return generatedValue[0];
        }
    }
}

﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class FloatGenerator : RandomNonDefaultGenerator<float>
    {
        protected override float GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[4];
            random.NextBytes(bytes);
            return BitConverter.ToSingle(bytes);
        }
    }
}

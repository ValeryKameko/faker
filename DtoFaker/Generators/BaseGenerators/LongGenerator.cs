﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class LongGenerator : RandomNonDefaultGenerator<long>
    {
        protected override long GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[8];
            random.NextBytes(bytes);
            return BitConverter.ToInt64(bytes);
        }
    }
}

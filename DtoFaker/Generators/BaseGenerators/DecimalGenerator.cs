﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class DecimalGenerator : RandomNonDefaultGenerator<decimal>
    {
        private const int DecimalGeneratorSteps = 28;
        private const double DecimalGeneratorProbability = 0.1;
        private const double DecimalNegativeProbability = 0.5;

        protected override decimal GenerateRandomValue(Random random)
        {
            int lo = random.Next();
            int mid = random.Next();
            int hi = random.Next();
            bool isNegative = random.NextDouble() >= DecimalNegativeProbability;
            byte scale = GenerateDecimalScale(random);
            return new decimal(lo, mid, hi, isNegative, scale);
        }

        private byte GenerateDecimalScale(Random random)
        {
            for (byte i = 0; i < DecimalGeneratorSteps; i++)
            {
                if (random.NextDouble() >= DecimalGeneratorProbability)
                    return i;
            }
            return 0;
        }
    }
}

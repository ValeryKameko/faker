﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class DoubleGenerator : RandomNonDefaultGenerator<double>
    {
        protected override double GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[8];
            random.NextBytes(bytes);
            return BitConverter.ToDouble(bytes);
        }
    }
}

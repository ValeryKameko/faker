﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class CharGenerator : RandomNonDefaultGenerator<char>
    {
        protected override char GenerateRandomValue(Random random) => (char)random.Next();
    }
}

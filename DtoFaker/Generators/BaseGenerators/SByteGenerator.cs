﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class SByteGenerator : RandomNonDefaultGenerator<sbyte>
    {
        protected override sbyte GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[1];
            random.NextBytes(bytes);
            return (sbyte)bytes[0];
        }
    }
}

﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class UIntGenerator : RandomNonDefaultGenerator<uint>
    {
        protected override uint GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[4];
            random.NextBytes(bytes);
            return BitConverter.ToUInt32(bytes);
        }
    }
}

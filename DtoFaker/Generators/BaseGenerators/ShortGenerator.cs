﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class ShortGenerator : RandomNonDefaultGenerator<short>
    {
        protected override short GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[2];
            random.NextBytes(bytes);
            return BitConverter.ToInt16(bytes);
        }
    }
}

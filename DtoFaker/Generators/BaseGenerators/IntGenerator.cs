﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class IntGenerator : RandomNonDefaultGenerator<int>
    {
        public int MinValue { get; private set; } = int.MinValue;
        public int MaxValue { get; private set; } = int.MaxValue;

        protected override int GenerateRandomValue(Random random) => random.Next(MinValue, MaxValue);
    }
}

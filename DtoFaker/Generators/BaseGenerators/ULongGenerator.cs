﻿using System;
using Faker.Lib;

namespace DtoFaker.Generators.BaseGenerators
{
    public sealed class ULongGenerator : RandomNonDefaultGenerator<ulong>
    {
        protected override ulong GenerateRandomValue(Random random)
        {
            byte[] bytes = new byte[8];
            random.NextBytes(bytes);
            return BitConverter.ToUInt64(bytes);
        }
    }
}

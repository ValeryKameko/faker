﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace DtoFaker.Generators.SpecialGenerators
{
    internal sealed class EnumGenerator : ConcreteTypeFakeGenerator<Enum>
    {
        private readonly Random _random = new Random();

        protected override object GenerateObject(Type type)
        {
            Array enumValues = type.GetEnumValues();
            int index = _random.Next(enumValues.GetLength(0));
            return enumValues.GetValue(index);
        }
    }
}

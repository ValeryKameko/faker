﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections;
using System.Linq;

namespace DtoFaker.Generators.SpecialGenerators
{
    public sealed class ArrayGenerator : ConcreteTypeFakeGenerator<Array>
    {
        private const int DefaultDimensionValue = 1;
        public int DimensionLength { get; set; } = DefaultDimensionValue;

        protected override object GenerateObject(Type type)
        {
            if (!type.IsArray)
                return null;

            int dimensionsCount = type.GetArrayRank();
            int[] dimensions = Enumerable.Repeat(DimensionLength, dimensionsCount).ToArray();

            Type elementType = type.GetElementType();
            Array generatedArray = Array.CreateInstance(elementType, dimensions);

            InitializeArray(generatedArray, dimensions);
            return generatedArray;
        }

        private void InitializeArray(Array generatedArray, int[] dimensions)
        {
            Type elementType = generatedArray.GetType().GetElementType();
            int[] index = (int[])Array.CreateInstance(typeof(int), dimensions.Length);
            do
            {
                object generatedElement = InnerTypeFaker.Generate(elementType);
                generatedArray.SetValue(generatedElement, index);
            } while (NextIndex(index, dimensions));
        }

        private static bool NextIndex(int[] index, int[] dimensions)
        {
            int carry = 0;
            index[0]++;
            for (int i = 0; i < index.Length; i++)
            {
                carry += index[i];
                index[i] = carry % dimensions[i];
                carry /= dimensions[i];
            }
            return carry == 0;
        }
    }
}

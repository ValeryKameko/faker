﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using DtoFaker.GeneratorRegistry;
using Faker.Lib;
using Faker.Lib.Generators;

namespace DtoFaker.Generators.SpecialGenerators
{
    internal sealed class CompositeGenerator : SpecialGenerator
    {
        public MemberGeneratorRegistry MemberGeneratorRegistry { get; set; }

        protected override object GenerateObject(Type type)
        {
            object value = CreateValue(type);
            if (value != null)
                InitializeValue(value);
            return value;
        }

        private object CreateValue(Type type)
        {
            IEnumerable<ConstructorInfo> constructors =
                from ConstructorInfo constructor in type.GetConstructors()
                orderby constructor.GetParameters().Length descending
                select constructor;
            foreach (ConstructorInfo constructor in constructors)
            {
                ParameterInfo[] parameters = constructor.GetParameters();
                IEnumerable<object> parameterValues = GenerateParameters(parameters);
                try
                {
                    object createdValue = constructor.Invoke(parameterValues.ToArray());
                    return createdValue;
                }
                catch (Exception)
                {
                    continue;
                }
            }
            if (IsStruct(type))
                return Activator.CreateInstance(type);
            return null;
        }

        private void InitializeValue(object value)
        {
            InitializeFields(value);
            InitializeProperties(value);
        }

        private void InitializeFields(object value)
        {
            Type type = value.GetType();
            IEnumerable<FieldInfo> fields =
                from FieldInfo field in type.GetFields()
                where IsDefaultValue(field.GetValue(value)) || MemberGeneratorRegistry.TryGetGenerator(field, out _)
                select field;
            foreach (FieldInfo field in fields)
            {
                object generatedValue;
                if (MemberGeneratorRegistry.TryGetGenerator(field, out FakeGenerator generator))
                    generatedValue = generator.Generate(field.FieldType);
                else
                    generatedValue = InnerTypeFaker.Generate(field.FieldType);
                field.SetValue(value, generatedValue);
            }
        }

        private void InitializeProperties(object value)
        {
            Type type = value.GetType();
            IEnumerable<PropertyInfo> properties =
                from PropertyInfo property in type.GetProperties()
                where property.CanWrite
                where !property.GetIndexParameters().Any()
                where !property.CanRead || IsDefaultValue(property.GetValue(value)) || MemberGeneratorRegistry.TryGetGenerator(property, out _)
                select property;
            foreach (PropertyInfo property in properties)
            {
                object generatedValue;
                if (MemberGeneratorRegistry.TryGetGenerator(property, out FakeGenerator generator))
                    generatedValue = generator.Generate(property.PropertyType);
                else
                    generatedValue = InnerTypeFaker.Generate(property.PropertyType);
                property.SetValue(value, generatedValue);
            }
        }

        private IEnumerable<object> GenerateParameters(IEnumerable<ParameterInfo> parameters)
        {
            foreach (ParameterInfo parameter in parameters)
            {
                if (MemberGeneratorRegistry.TryGetGeneratorByConstructorParameter(parameter, out FakeGenerator memberGenerator))
                    yield return memberGenerator.Generate(parameter.ParameterType);
                else
                    yield return InnerTypeFaker.Generate(parameter.ParameterType);
            }
        }

        private bool IsStruct(Type type) => type.IsValueType && !type.IsEnum;

        private static bool IsDefaultValue(object value)
        {
            if (value == null)
                return true;
            Type type = value.GetType();
            object defaultValue = type.IsValueType ? Activator.CreateInstance(type) : null;
            return value == null || value.Equals(defaultValue);
        }
    }
}

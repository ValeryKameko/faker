﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace DtoFaker.Generators.SpecialGenerators
{
    internal sealed class DefaultGenerator : SpecialGenerator
    {
        protected override object GenerateObject(Type type)
        {
            return type.IsValueType ? Activator.CreateInstance(type) : null;
        }
    }
}

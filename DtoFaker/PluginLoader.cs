﻿using Faker.Lib;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DtoFaker
{
    using Faker = Faker.Lib.Faker;

    internal static class PluginLoader<TPlugin>
    {
        public static IEnumerable<Type> LoadPluginTypes(string directory, string extension, SearchOption searchOption)
        {
            IEnumerable<Type> types = Enumerable.Empty<Type>();
            string dir = Directory.GetCurrentDirectory();
            if (Directory.Exists(directory))
            {
                foreach (string filePath in Directory.EnumerateFiles(directory, $"*.{extension}", searchOption))
                {
                    Assembly assembly = Assembly.LoadFile(Path.GetFullPath(filePath));
                    if (assembly == null)
                        continue;
                    IEnumerable<Type> assemblyTypes =
                        from Type type in assembly.ExportedTypes
                        where !type.IsAbstract && !type.IsInterface
                        where typeof(TPlugin).IsAssignableFrom(type)
                        select type;
                    types = types.Concat(assemblyTypes);
                }
            }
            return types;
        }
    }
}

﻿using DtoFaker.GeneratorRegistry;
using DtoFaker.Generators.BaseGenerators;
using Faker.Lib.Generators;
using System;
using DtoFaker.Generators.SpecialGenerators;
using DtoFaker.Generators.GenericGenerators;
using System.Collections.Generic;
using System.Reflection;
using System.IO;

namespace DtoFaker
{
    public sealed class DtoFaker : Faker.Lib.Faker
    {
        private readonly IFakeGeneratorRegistry _concreteTypeRegistry = new ConcreteTypeFakeGeneratorRegistry();
        private readonly IFakeGeneratorRegistry _genericTypeRegistry = new GenericTypeFakeGeneratorRegistry();
        private readonly MemberGeneratorRegistry _memberGeneratorRegistry;

        private readonly FakeGenerator _arrayGenerator;
        private readonly FakeGenerator _enumGenerator;
        private readonly CompositeGenerator _compositeGenerator;
        private readonly FakeGenerator _defaultGenerator;

        private readonly ISet<Type> _generatingTypes = new HashSet<Type>();

        public DtoFaker(DtoFakerConfig config)
        {
            _memberGeneratorRegistry = new MemberGeneratorRegistry(this, config.GeneratorFactoryByMember);
            RegisterGenerator<BoolGenerator>();
            RegisterGenerator<CharGenerator>();
            RegisterGenerator<ByteGenerator>();
            RegisterGenerator<SByteGenerator>();
            RegisterGenerator<ShortGenerator>();
            RegisterGenerator<UShortGenerator>();
            RegisterGenerator<IntGenerator>();
            RegisterGenerator<UIntGenerator>();
            RegisterGenerator<LongGenerator>();
            RegisterGenerator<ULongGenerator>();
            RegisterGenerator<FloatGenerator>();
            RegisterGenerator<DoubleGenerator>();
            RegisterGenerator<DecimalGenerator>();
            _arrayGenerator = RegisterGenerator<ArrayGenerator>();
            RegisterGenerator<ListGenerator>();
            _enumGenerator = RegisterGenerator<EnumGenerator>();
            _compositeGenerator = CreateGeneratorInstance<CompositeGenerator>();
            _compositeGenerator.MemberGeneratorRegistry = _memberGeneratorRegistry;
            _defaultGenerator = CreateGeneratorInstance<DefaultGenerator>();

            LoadGenerators(PluginLoader<FakeGenerator>.LoadPluginTypes(config.Directory, config.Extension, config.SearchOption));
        }

        public DtoFaker()
            : this(new DtoFakerConfig())
        {
        }

        protected override object GenerateObject(Type type)
        {
            var generatingType = type;
            if (type.IsArray)
                return _arrayGenerator.Generate(type);
            if (_concreteTypeRegistry.TryGetGenerator(generatingType, out FakeGenerator concreteGenerator))
                return concreteGenerator.Generate(type);
            if (_genericTypeRegistry.TryGetGenerator(generatingType, out FakeGenerator genericGenerator))
                return genericGenerator.Generate(type);
            if (type.IsEnum)
                return _enumGenerator.Generate(type);
            if (type.IsPrimitive)
                return Activator.CreateInstance(type);
            if (type.IsValueType && !type.IsEnum || type.IsClass)
            {
                try
                {
                    if (_generatingTypes.Contains(type))
                        return _defaultGenerator.Generate(type);
                    _generatingTypes.Add(type);
                    return _compositeGenerator.Generate(type);
                }
                finally
                {
                    _generatingTypes.Remove(type);
                }
            }
            return _defaultGenerator.Generate(type);
        }

        protected override void RegisterGenerator(FakeGenerator generator)
        {
            _concreteTypeRegistry.RegisterGenerator(generator);
            _genericTypeRegistry.RegisterGenerator(generator);
        }

        private void LoadGenerators(IEnumerable<Type> types)
        {
            foreach (var type in types)
            {
                RegisterGenerator(type);
            }
        }
    }
}

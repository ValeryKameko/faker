﻿using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Linq.Expressions;
using System.Reflection;
using System.Text;

namespace DtoFaker
{
    using Faker = Faker.Lib.Faker;

    public sealed class DtoFakerConfig
    {
        public const string DefaultExtension = "dll";
        public const string DefaultDirectory = "plugins";
        public const SearchOption DefaultSearchOption = SearchOption.AllDirectories;

        public string Extension { get; set; } = DefaultExtension;
        public string Directory { get; set; } = DefaultDirectory;
        public SearchOption SearchOption { get; set; } = DefaultSearchOption;

        private readonly Dictionary<MemberInfo, Func<Faker, FakeGenerator>> _generatorFactoryByMember
            = new Dictionary<MemberInfo, Func<Faker, FakeGenerator>>();

        internal IReadOnlyDictionary<MemberInfo, Func<Faker, FakeGenerator>> GeneratorFactoryByMember => _generatorFactoryByMember;

        public void Add<TClass, TMemberType, TGenerator>(Expression<Func<TClass, TMemberType>> expression)
            where TClass : class
            where TGenerator : FakeGenerator, new()
        {
            if (!(expression.Body is MemberExpression memberExpression))
            {
                throw new ArgumentException("Expression body must be MemberAccess expression");
            }
            MemberInfo member = memberExpression.Member;
            if (!new MemberTypes[] { MemberTypes.Field, MemberTypes.Property }.Contains(member.MemberType))
            {
                throw new ArgumentException("Member must be field or property");
            }
            _generatorFactoryByMember.Add(member, faker => faker.CreateGeneratorInstance<TGenerator>());
        }
    }
}

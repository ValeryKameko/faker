﻿using Faker.Lib.Generators;
using System;
using System.Collections.Generic;

namespace DtoFaker.GeneratorRegistry
{
    internal interface IFakeGeneratorRegistry
    {
        void RegisterGenerator(FakeGenerator generator);
        bool TryGetGenerator(Type type, out FakeGenerator generator);
    }
}
﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Linq;
using System.Collections.Generic;

namespace DtoFaker.GeneratorRegistry
{
    internal sealed class ConcreteTypeFakeGeneratorRegistry : IFakeGeneratorRegistry
    {
        private readonly Dictionary<Type, FakeGenerator> _generatorByType = new Dictionary<Type, FakeGenerator>();

        public void RegisterGenerator(FakeGenerator generator)
        {
            Type keyType = generator.SupportedType;
            foreach (Type interfaceType in from Type type in keyType.GetInterfaces()
                                           select type)
            {
                _generatorByType[interfaceType] = generator;
            }
            _generatorByType[keyType] = generator;
            for (Type baseType = keyType.BaseType; baseType != null; baseType = baseType.BaseType)
            {
                if (baseType.IsAbstract)
                {
                    _generatorByType[baseType] = generator;
                }
            }
        }

        public bool TryGetGenerator(Type type, out FakeGenerator generator)
        {
            return _generatorByType.TryGetValue(type, out generator);
        }
    }
}

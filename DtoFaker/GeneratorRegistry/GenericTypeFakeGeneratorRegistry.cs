﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Linq;

namespace DtoFaker.GeneratorRegistry
{
    public class GenericTypeFakeGeneratorRegistry : IFakeGeneratorRegistry
    {
        private class GenericTypeGeneratorInvoker : FakeGenerator
        {
            private readonly FakeGenerator _generator;
            private readonly Type _transitType;

            public GenericTypeGeneratorInvoker(FakeGenerator generator, Type transitType)
            {
                _generator = generator;
                _transitType = transitType;
            }

            public override Type SupportedType => null;

            protected override object GenerateObject(Type generatingType)
            {
                Type supportedType = _generator.SupportedType;
                Dictionary<Type, Type> transitToGeneratingMapping = CreateTypeMapping(_transitType, generatingType);

                IEnumerable<Type> supportedTypeArguments = supportedType.GetGenericArguments();
                Type supportedTypeDefinition = supportedType.GetGenericTypeDefinition();

                IEnumerable<Type> targetTypeArguments =
                    supportedTypeArguments.Select(argument => argument.IsGenericParameter ? transitToGeneratingMapping[argument] : argument);
                Type targetType = supportedTypeDefinition.MakeGenericType(targetTypeArguments.ToArray());
                return _generator.Generate(targetType);
            }

            private Dictionary<Type, Type> CreateTypeMapping(Type fromType, Type toType)
            {
                IEnumerable<Type> fromTypeArguments = fromType.GetGenericArguments();
                IEnumerable<Type> toTypeArguments = toType.GetGenericArguments();
                Func<Type, Type, KeyValuePair<Type, Type>> pairsCreator =
                    (fromArgument, toArgument) => new KeyValuePair<Type, Type>(fromArgument, toArgument);
                return fromTypeArguments
                        .Zip(toTypeArguments, pairsCreator)
                        .Where(item => item.Key.IsGenericParameter)
                        .ToDictionary(item => item.Key, item => item.Value);
            }
        }

        private class RegisteredGenerator
        {
            public RegisteredGenerator(Type generatingType, FakeGenerator generator)
            {
                GeneratingType = generatingType;
                Generator = generator;
            }

            public Type GeneratingType { get; }
            public FakeGenerator Generator { get; }
        }

        private readonly Dictionary<Type, List<RegisteredGenerator>> _generatorsByType =
            new Dictionary<Type, List<RegisteredGenerator>>();

        public void RegisterGenerator(FakeGenerator generator)
        {
            Type keyType = generator.SupportedType;
            foreach (Type interfaceType in from Type type in keyType.GetInterfaces()
                                           where type.IsGenericType
                                           select type)
            {
                RegisterGenerator(interfaceType, generator);
            }

            for (Type baseType = keyType; baseType != null; baseType = baseType.BaseType)
            {
                if (baseType.IsGenericType)
                {
                    RegisterGenerator(baseType, generator);
                }
            }
        }

        public bool TryGetGenerator(Type type, out FakeGenerator generator)
        {
            generator = null;
            if (!type.IsGenericType || !type.IsConstructedGenericType)
                return false;
            Type typeDefinition = type.GetGenericTypeDefinition();
            if (!_generatorsByType.TryGetValue(typeDefinition, out List<RegisteredGenerator> generators))
                return false;

            foreach (RegisteredGenerator registeredGenerator in ReverseList(generators))
            {
                if (IsTypesCompatible(type, registeredGenerator.GeneratingType))
                {
                    generator = new GenericTypeGeneratorInvoker(registeredGenerator.Generator, registeredGenerator.GeneratingType);
                    return true;
                }
            }
            return false;
        }

        private void RegisterGenerator(Type generatingType, FakeGenerator generator)
        {
            Type generatingTypeDefinition = generatingType.GetGenericTypeDefinition();
            var registeredGenerator = new RegisteredGenerator(generatingType, generator);
            if (_generatorsByType.ContainsKey(generatingTypeDefinition))
                _generatorsByType[generatingTypeDefinition].Add(registeredGenerator);
            else
                _generatorsByType[generatingTypeDefinition] = new List<RegisteredGenerator>() { registeredGenerator };
        }

        private static IEnumerable<T> ReverseList<T>(IList<T> list)
        {
            for (int i = list.Count - 1; i >= 0; i--)
            {
                yield return list[i];
            }
        }

        private static bool IsTypesCompatible(Type from, Type to)
        {
            if (from.GetGenericTypeDefinition() != to.GetGenericTypeDefinition())
                return false;
            IEnumerable<Type> fromArguments = from.GetGenericArguments();
            IEnumerable<Type> toArguments = to.GetGenericArguments();
            return fromArguments.Zip(toArguments, IsTypeArgumentsCompatible).All(condition => condition);
        }

        private static bool IsTypeArgumentsCompatible(Type fromArgument, Type toArgument)
        {
            bool isFromGeneric = fromArgument.IsGenericParameter;
            bool isToGeneric = toArgument.IsGenericParameter;
            if (isFromGeneric || isToGeneric)
                return true;
            return fromArgument == toArgument;
        }
    }
}

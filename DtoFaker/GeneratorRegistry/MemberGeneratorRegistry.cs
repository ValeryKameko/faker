﻿using DtoFaker.Generators.SpecialGenerators;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace DtoFaker.GeneratorRegistry
{
    using Faker = Faker.Lib.Faker;

    internal sealed class MemberGeneratorRegistry
    {
        private readonly IDictionary<MemberInfo, FakeGenerator> _memberToGenerator
            = new Dictionary<MemberInfo, FakeGenerator>();
        private readonly IReadOnlyDictionary<MemberInfo, Func<Faker, FakeGenerator>> _generatorFactoryByMember;
        private readonly Faker _faker;

        public MemberGeneratorRegistry(Faker faker, IReadOnlyDictionary<MemberInfo, Func<Faker, FakeGenerator>> generatorFactoryByMember)
        {
            _faker = faker;
            _generatorFactoryByMember = generatorFactoryByMember;
        }

        public bool TryGetGenerator(MemberInfo member, out FakeGenerator generator)
        {
            if (_memberToGenerator.TryGetValue(member, out generator))
                return true;
            if (!_generatorFactoryByMember.TryGetValue(member, out Func<Faker, FakeGenerator> memberGeneratorFactory))
                return false;
            generator = memberGeneratorFactory.Invoke(_faker);
            _memberToGenerator.Add(member, generator);
            return true;
        }

        public bool TryGetGeneratorByConstructorParameter(ParameterInfo parameter, out FakeGenerator generator)
        {
            generator = null;
            MemberInfo typeMember = GetFieldOrPropertyMemberByParameter(parameter);
            if (typeMember == null)
                return false;
            if (_memberToGenerator.TryGetValue(typeMember, out generator))
                return true;
            if (!_generatorFactoryByMember.TryGetValue(typeMember, out Func<Faker, FakeGenerator> memberGeneratorFactory))
                return false;
            generator = memberGeneratorFactory.Invoke(_faker);
            _memberToGenerator.Add(typeMember, generator);
            return true;
        }

        private string MakeCamelCaseName(string pascalCaseName) => char.ToUpper(pascalCaseName[0]) + pascalCaseName.Substring(1);

        private MemberInfo GetFieldOrPropertyMemberByParameter(ParameterInfo parameter)
        {
            string parameterName = parameter.Name;
            string memberName = MakeCamelCaseName(parameter.Name);
            Type parameterType = parameter.ParameterType;
            Type type = parameter.Member.DeclaringType;

            IEnumerable<MemberInfo> properties = GetProperties(type, memberName, parameterType);
            MemberInfo typeMember = properties.FirstOrDefault();
            return properties.Count() == 1 ? typeMember : null;
        }

        private IEnumerable<MemberInfo> GetProperties(Type type, string name, Type propertyType)
        {
            return from PropertyInfo property in type.GetProperties()
                   where property.Name == name
                   where property.PropertyType == propertyType
                   select property;
        }
    }
}

﻿using Faker.Lib.Generators;
using System;

namespace Generators.DateTimeGenerator
{
    public sealed class DateTimeGenerator : ConcreteTypeFakeGenerator<DateTime>
    {
        protected override object GenerateObject(Type type) => DateTime.Now;
    }
}

﻿using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerCollectionTest
    {
        Faker.Lib.Faker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [TestCase(typeof(int[]))]
        [TestCase(typeof(bool[,]))]
        [TestCase(typeof(long[][]))]
        public void GenericGenerate_WhenTypeIsArray_ShouldCreateNonDefaultValue(Type type)
        {
            object value = _faker.InvokeFakerGenerate(type);
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value));
        }

        [TestCase(typeof(int[]))]
        [TestCase(typeof(bool[,]))]
        [TestCase(typeof(long[][]))]
        public void Generate_WhenTypeIsArray_ShouldCreateNonDefaultValue(Type type)
        {
            object value = _faker.Generate(type);
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value));
        }

        [TestCase(typeof(ICollection<int>))]
        [TestCase(typeof(IEnumerable<int>))]
        [TestCase(typeof(List<int>))]
        [TestCase(typeof(IList<int>))]
        [TestCase(typeof(IList<IList<int>>))]
        public void Generate_WhenTypeIsCollection_ShouldCreateNonDefaultValue(Type type)
        {
            object value = _faker.InvokeFakerGenerate(type);
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value));
        }
    }
}

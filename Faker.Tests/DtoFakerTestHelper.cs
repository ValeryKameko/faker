﻿using Faker.Lib;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;

namespace Faker.Tests
{
    internal static class DtoFakerTestHelper
    {
        public static bool IsNonDefaultValue(object value)
        {
            Type type = value.GetType();
            object defaultValue = GetDefaultValue(type);
            if (type.IsValueType)
            {
                return !defaultValue.Equals(value);
            }
            if (value == null || value.Equals(defaultValue))
                return false;
            if (HasDefaultPublicFields(value))
                return false;
            if (HasDefaultPublicProperties(value))
                return false;
            if (!IsNonDefaultEnumerable(value))
                return false;
            return true;
        }

        public static object InvokeFakerGenerate(this IFaker faker, Type type)
        {
            var generateMethod = faker.GetType().GetMethod("Generate", 1, new Type[] { });
            return generateMethod.MakeGenericMethod(type).Invoke(faker, new object[] { });
        }


        private static object GetDefaultValue(Type type) => type.IsValueType ? Activator.CreateInstance(type) : null;

        private static bool HasDefaultPublicFields(object value)
        {
            Type type = value.GetType();
            foreach (object fieldValue in from FieldInfo field in type.GetFields()
                                          select field.GetValue(value))
            {
                if (!IsNonDefaultValue(fieldValue))
                    return true;
            }
            return false;
        }

        public static bool IsNonDefaultEnumerable(object value)
        {
            if (value is IEnumerable collection)
            {
                return collection.Cast<object>().Any(IsNonDefaultValue);
            }
            return true;
        }

        private static bool HasDefaultPublicProperties(object value)
        {
            Type type = value.GetType();
            foreach (object propertyValue in from PropertyInfo property in type.GetProperties()
                                             where IsCorrectProperty(property)
                                             select property.GetValue(value))
            {
                if (!IsNonDefaultValue(propertyValue))
                    return true;
            }
            return false;
        }

        private static bool IsCorrectProperty(PropertyInfo property)
        {
            bool result = property.GetMethod != null && property.GetMethod.IsPublic;
            result = result && property.SetMethod != null && property.SetMethod.IsPublic;
            result = result && !property.GetIndexParameters().Any();
            return result;
        }
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Types
{
    public class SimpleGenericClass<T>
    {
        public SimpleGenericClass(T value) => ReadonlyProperty = value;

        public T PublicProperty { get; set; }
        public T ReadonlyProperty { get; }
        public T field;
    }

    public class RecursiveGenericClass<T>
    {
        public RecursiveGenericClass(RecursiveGenericClass<T> value) => field = value;
        public RecursiveGenericClass<T> field;
    }

    public class ComplexGenericClass<T, U>
    {
        public ComplexGenericClass(T tValue, U uValue)
        {
            tField = tValue;
            uField = uValue;
        }
        public T tField;
        public U uField;
    }
}

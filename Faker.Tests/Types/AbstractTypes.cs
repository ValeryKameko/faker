﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Types
{
    internal abstract class AbstractClassType
    {
        public int field;
        public long Proprerty { get; set; }
        public abstract void AbstractMethod();
    }

    internal interface IInterfaceType
    {
        long Property { get; set; }
        void InterfaceMethod();
    }
}

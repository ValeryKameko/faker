﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Types
{
    internal enum EnumType
    {
        EnumValue1,
        EnumValue2,
        EnumValue3,
        EnumValue4
    }
}

﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Types
{
    internal struct SimpleStructType
    {
        public int field1;
        public double field2;
        private int privateField3;
    }

    internal struct ComplexStructType
    {
        public long field1;
        public decimal field2;
        public SimpleStructType complexField1;
        public SimpleStructType complexField2;
    }

    internal struct StructWithPropertiesType
    {
        public int Property1 { get; set; }
        public double ReadonlyProperty2 { get; private set; }
        public SimpleStructType WriteonlyProperty3 { private get; set; }
        private int PrivateProperty4 { get; set; }
    }
}

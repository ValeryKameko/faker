﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Types
{
    internal class SimpleClassType
    {
        public int field1;
        public IEnumerable<int> field2;
    }

    internal class ComplexClassType
    {
        public SimpleClassType field1;
        public SimpleClassType[] field2;
        public IEnumerable<SimpleClassType> field3;
    }

    internal class SimpleRecursiveClassType
    {
        public SimpleRecursiveClassType field;
        public SimpleRecursiveClassType Property { get; set; }
    }

    internal class InnerIndirectRecursiveClassType
    {
        public IndirectRecursiveClassType field;
    }

    internal class IndirectRecursiveClassType
    {
        public InnerIndirectRecursiveClassType field;
    }

    internal class CollectionRecursiveClassType
    {
        public CollectionRecursiveClassType[] field1;
        public List<CollectionRecursiveClassType> field2;
        public IEnumerable<CollectionRecursiveClassType> field3;
    }

    internal class PrivateConstructorClassType
    {
        private PrivateConstructorClassType()
        {
        }
    }

    internal class LongConstructorClassType
    {
        public LongConstructorClassType(int x) => X = x;

        public LongConstructorClassType(int x, int y)
        {
            X = x;
            Y = y;
        }

        public int X { get; }
        public int Y { get; }
    }

    internal class ClassWithPublicField
    {
        public int field;
    }

    internal class ClassWithPublicProperty
    {
        public int Property { get; set; }
    }

    internal class ClassWithReadonlyProperty
    {
        public int field;
        public int Field { get; }
        public ClassWithReadonlyProperty(int field) => Field = field;
    }
}

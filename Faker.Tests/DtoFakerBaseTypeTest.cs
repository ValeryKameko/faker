using Faker.Lib;
using NUnit.Framework;
using System;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerBaseTypeTest
    {
        Faker.Lib.Faker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [TestCase(typeof(bool))]
        [TestCase(typeof(byte))]
        [TestCase(typeof(sbyte))]
        [TestCase(typeof(char))]
        [TestCase(typeof(short))]
        [TestCase(typeof(ushort))]
        [TestCase(typeof(int))]
        [TestCase(typeof(uint))]
        [TestCase(typeof(long))]
        [TestCase(typeof(ulong))]
        [TestCase(typeof(float))]
        [TestCase(typeof(decimal))]
        [TestCase(typeof(double))]
        public void GenericGenerate_WhenTypeIsPrimitive_ShouldCreateNonDefaultValue(Type type)
        {
            object generatedValueByGenericMethod = _faker.InvokeFakerGenerate(type);
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(generatedValueByGenericMethod));
        }

        [TestCase(typeof(bool))]
        [TestCase(typeof(byte))]
        [TestCase(typeof(sbyte))]
        [TestCase(typeof(char))]
        [TestCase(typeof(short))]
        [TestCase(typeof(ushort))]
        [TestCase(typeof(int))]
        [TestCase(typeof(uint))]
        [TestCase(typeof(long))]
        [TestCase(typeof(ulong))]
        [TestCase(typeof(float))]
        [TestCase(typeof(decimal))]
        [TestCase(typeof(double))]
        public void Generate_WhenTypeIsPrimitive_ShouldCreateNonDefaultValue(Type type)
        {
            object generatedValueByGenericMethod = _faker.Generate(type);
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(generatedValueByGenericMethod));
        }
    }
}
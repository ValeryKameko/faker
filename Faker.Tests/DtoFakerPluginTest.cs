﻿using Faker.Lib;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerPluginTest
    {
        private IFaker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsStringPlugin_ShouldGenerateValueUsingPlugin()
        {
            var value = _faker.Generate<string>();
            Assert.IsTrue(Guid.TryParse(value, out _));
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsDateTimePlugin_ShouldGenerateValueUsingPlugin()
        {
            var value = _faker.Generate<DateTime>();

            Assert.AreEqual(value.Year, DateTime.Now.Year, 1.01);
        }

    }
}

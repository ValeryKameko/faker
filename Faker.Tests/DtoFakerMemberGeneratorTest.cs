﻿using DtoFaker;
using Faker.Lib;
using Faker.Tests.Generators;
using Faker.Tests.Types;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    using DtoFaker = DtoFaker.DtoFaker;

    [TestFixture]
    public class DtoFakerMemberGeneratorTest
    {
        [Test]
        public void GenericGenerate_WhenFieldGeneratorIsRegistered_ShouldCreateMemberUsingCustomGenerator()
        {
            var config = new DtoFakerConfig();
            config.Add<ClassWithPublicField, int, SimpleGenerator>(instance => instance.field);
            IFaker faker = new DtoFaker(config);

            var value = faker.Generate<ClassWithPublicField>();
            Assert.AreEqual(SimpleGenerator.GeneratingValue, value.field);
        }

        [Test]
        public void GenericGenerate_WhenPropertyGeneratorIsRegistered_ShouldCreateMemberUsingCustomGenerator()
        {
            var config = new DtoFakerConfig();
            config.Add<ClassWithPublicProperty, int, SimpleGenerator>(instance => instance.Property);
            IFaker faker = new DtoFaker(config);

            var value = faker.Generate<ClassWithPublicProperty>();
            Assert.AreEqual(SimpleGenerator.GeneratingValue, value.Property);
        }

        [Test]
        public void GenericGenerate_WhenPropertyGeneratorIsRegistered_ShouldCreatePrivateMemberThroughConstructor()
        {
            var config = new DtoFakerConfig();
            config.Add<ClassWithReadonlyProperty, int, SimpleGenerator>(instance => instance.Field);
            IFaker faker = new DtoFaker(config);

            var value = faker.Generate<ClassWithReadonlyProperty>();
            Assert.AreEqual(SimpleGenerator.GeneratingValue, value.Field);
        }
    }
}

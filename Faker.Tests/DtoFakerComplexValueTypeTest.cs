﻿using Faker.Tests.Types;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerComplexValueTypeTest
    {
        Faker.Lib.Faker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void Generate_WhenTypeIsEnum_ShouldCreateValue()
        {
            object value = _faker.Generate(typeof(EnumType));
            Assert.IsNotNull(value);
            Assert.AreEqual(typeof(EnumType), value.GetType());
        }

        [Test]
        public void Generate_WhenTypeIsStruct_ShouldCreateValue()
        {
            object value = _faker.Generate(typeof(SimpleStructType));
            Assert.IsNotNull(value);
            Assert.AreEqual(typeof(SimpleStructType), value.GetType());

            SimpleStructType simpleStruct = (SimpleStructType)value;
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(simpleStruct.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(simpleStruct.field2));
        }

        [Test]
        public void GenericGenerate_WhenTypeIsComplexStruct_ShouldCreateValue()
        {
            var value = _faker.Generate<ComplexStructType>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field2));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.complexField1.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.complexField1.field2));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.complexField2.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.complexField2.field2));
        }

        [Test]
        public void GenericGenerate_WhenTypeIsStructWithProperties_ShouldCreateValue()
        {
            var value = _faker.Generate<StructWithPropertiesType>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.Property1));
        }
    }
}

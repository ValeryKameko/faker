﻿using Faker.Lib;
using NUnit.Framework;
using Faker.Tests.Types;
using System.Runtime.Serialization.Json;
using System;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerClassTest
    {
        private IFaker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void GenericGenerate_WhenTypeIsSimpleClass_ShouldCreateValue()
        {
            var value = _faker.Generate<SimpleClassType>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field2));
        }

        [Test]
        public void GenericGenerate_WhenTypeIsComplexClass_ShouldCreateValue()
        {
            var value = _faker.Generate<ComplexClassType>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field1));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field2));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field3));
        }

        [Test]
        public void GenericGenerate_WhenTypeIsSimpleRecursiveClass_ShouldCreateValue()
        {
            var value = _faker.Generate<SimpleRecursiveClassType>();
            Assert.IsNotNull(value);

            Assert.IsNull(value.field);
            Assert.IsNull(value.Property);
        }

        [Test]
        public void GenericGenerate_WhenTypeIsIndirectRecursiveClass_ShouldCreateValue()
        {
            var value = _faker.Generate<IndirectRecursiveClassType>();
            Assert.IsNotNull(value);

            Assert.IsNotNull(value.field);
            Assert.IsNull(value.field.field);
        }

        [Test]
        public void GenericGenerate_WhenTypeIsCollectionRecursiveClass_ShouldCreateValue()
        {
            var value = _faker.Generate<CollectionRecursiveClassType>();
            Assert.IsNotNull(value);

            Assert.IsNotNull(value.field1);
            Assert.That(value.field1, Is.All.Null);
            Assert.IsNotNull(value.field2);
            Assert.That(value.field2, Is.All.Null);
            Assert.IsNotNull(value.field3);
            Assert.That(value.field3, Is.All.Null);
        }

        [Test]
        public void GenericGenerate_WhenTypeIsClassWithPrivateConstructor_ShouldNotCreateValue()
        {
            var value = _faker.Generate<PrivateConstructorClassType>();
            Assert.IsNull(value);            
        }

        [Test]
        public void GenericGenerate_WhenTypeIsClassWithSeveralConstructor_ShouldCreateValueUsingLongestConstructor()
        {
            var value = _faker.Generate<LongConstructorClassType>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.X));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.Y));
        }

        [TestCase(typeof(DataContractJsonSerializer))]
        [TestCase(typeof(Type))]
        public void GenericGenerate_WhenTypeIsNotConstructable_ShouldNotThrow(Type type)
        {
            Assert.DoesNotThrow(() => _faker.Generate(type));
        }
    }
}

﻿using Faker.Lib;
using Faker.Tests.Types;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public  class DtoFakerAbstractTypeTest
    {
        private IFaker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void GenericGenerate_WhenTypeIsAbstractClass_ShouldNotCreateValue()
        {
            var value = _faker.Generate<AbstractClassType>();
            Assert.IsNull(value);
        }

        [Test]
        public void GenericGenerate_WhenTypeIsInterfaceType_ShouldNotCreateValue()
        {
            var value = _faker.Generate<IInterfaceType>();
            Assert.IsNull(value);
        }
    }
}

﻿using Faker.Tests.Generators;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerGeneratorTest
    {
        DtoFaker.DtoFaker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsegistered_ShouldCreateValueUsingCustomGenerator()
        {
            _faker.RegisterGenerator<SimpleGenerator>();
            var value = _faker.Generate<int>();
            Assert.AreEqual(value, SimpleGenerator.GeneratingValue);
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsGeneric_ShouldCreateGenericValue()
        {
            _faker.RegisterGenerator<DictionaryGenericGenerator>();
            var value = _faker.Generate<Dictionary<int, double>>();
            Assert.IsNotNull(value);

            Assert.That(value.Keys, Is.All.Not.Null);
            Assert.That(value.Values, Is.All.Not.Null);
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsGeneric_ShouldCreateIndirectGenericValue()
        {
            _faker.RegisterGenerator<DictionaryGenericGenerator>();
            var value = _faker.Generate<IReadOnlyDictionary<int, double>>();
            Assert.IsNotNull(value);

            Assert.That(value.Keys, Is.All.Not.Null);
            Assert.That(value.Values, Is.All.Not.Null);
        }

        [Test]
        public void GenericGenerate_WhenGeneratorIsPartialGeneric_ShouldCreateGenericValue()
        {
            _faker.RegisterGenerator<IntDictionaryGenericGenerator>();
            var value = _faker.Generate<Dictionary<int, double>>();
            Assert.IsNotNull(value);

            CollectionAssert.AreEqual(value.Keys.ToList(), Enumerable.Range(0, 3).ToList());
        }
    }
}

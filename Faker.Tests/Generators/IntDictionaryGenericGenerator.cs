﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Generators
{
    internal class IntDictionaryGenericGenerator : FakeGenerator
    {
        private const int DefaultSizeValue = 3;

        private static readonly Type GenericDictionaryType = typeof(Dictionary<,>);
        private static readonly Type KeyType = typeof(int);
        private static readonly Type ValueType = GenericDictionaryType.GetGenericArguments()[1];
        private static readonly Type SupportedTypeValue = GenericDictionaryType.GetGenericTypeDefinition().MakeGenericType(KeyType, ValueType);

        public int Size { get; set; } = DefaultSizeValue;

        public override Type SupportedType => SupportedTypeValue;

        protected override object GenerateObject(Type type)
        {
            object dictionary = Activator.CreateInstance(type);
            InitializeIntDictionary((dynamic)dictionary);
            return dictionary;
        }

        private void InitializeIntDictionary<TValue>(Dictionary<int, TValue> dictionary)
        {
            for (int i = 0; i < Size; i++)
            {
                int key = i;
                TValue value = InnerTypeFaker.Generate<TValue>();
                dictionary.Add(key, value);
            }
        }
    }
}

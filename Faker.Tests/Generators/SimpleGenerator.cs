﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Generators
{
    internal class SimpleGenerator : ConcreteTypeFakeGenerator<int>
    {
        public const int GeneratingValue = 228;

        protected override object GenerateObject(Type type) => GeneratingValue;
    }
}

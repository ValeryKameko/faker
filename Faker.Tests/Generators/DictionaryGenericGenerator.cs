﻿using Faker.Lib;
using Faker.Lib.Generators;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests.Generators
{
    internal class DictionaryGenericGenerator : FakeGenerator
    {
        private const int DefaultSizeValue = 3;
        public int Size { get; set; } = DefaultSizeValue;

        public override Type SupportedType => typeof(Dictionary<,>);

        protected override object GenerateObject(Type type)
        {
            object dictionary = Activator.CreateInstance(type);
            InitializeDictionary((dynamic)dictionary);
            return dictionary;
        }

        private void InitializeDictionary<TKey, TValue>(Dictionary<TKey, TValue> dictionary)
        {
            for (int i = 0; i < Size; i++)
            {
                TKey key = InnerTypeFaker.Generate<TKey>();
                TValue value = InnerTypeFaker.Generate<TValue>();
                dictionary.Add(key, value);
            }
        }
    }
}

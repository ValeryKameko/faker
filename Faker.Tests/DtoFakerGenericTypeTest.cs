﻿using Faker.Tests.Types;
using NUnit.Framework;
using System;
using System.Collections.Generic;
using System.Text;

namespace Faker.Tests
{
    [TestFixture]
    public class DtoFakerGenericTypeTest
    {
        Faker.Lib.Faker _faker;

        [SetUp]
        public void Setup()
        {
            _faker = new DtoFaker.DtoFaker();
        }

        [Test]
        public void GenericGenerate_WhenTypeIsSimpleGeneric_ShouldCreateValue()
        {
            var value = _faker.Generate<SimpleGenericClass<int>>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.ReadonlyProperty));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.field));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.PublicProperty));
        }

        [Test]
        public void GenericGenerate_WhenTypeIsRecursiveGeneric_ShouldCreateValueButNotField()
        {
            var value = _faker.Generate<RecursiveGenericClass<double>>();
            Assert.IsNotNull(value);

            Assert.IsNull(value.field);
        }

        [Test]
        public void GenericGenerate_WhenTypeIsComplexGeneric_ShouldCreateValue()
        {
            var value = _faker.Generate<ComplexGenericClass<double, int>>();
            Assert.IsNotNull(value);

            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.tField));
            Assert.IsTrue(DtoFakerTestHelper.IsNonDefaultValue(value.uField));
        }
    }
}
